﻿using System;
using System.IO;
using System.Net;

namespace FarmModManagerLauncher.core
{
    class Server
    {
        /*public bool versionCheck(string currentVersion)
        {
            WebRequest versionCheck = WebRequest.Create(new Uri("https://cattleandmods.de/fmml/version.json"))
        }*/

       public string downloadFile(String remoteFilename,
                               String localFilename)
        {
            int bytesProcessed = 0;

            Stream remoteStream  = null;
            Stream localStream   = null;
            WebResponse response = null;


            try
            {
                WebRequest request = WebRequest.Create(remoteFilename);

                if ( request != null )
                {
                    response = request.GetResponse();

                    if ( response != null )
                    {
                        remoteStream = response.GetResponseStream();

                        localStream = File.Create(localFilename);

                        byte[] buffer = new byte[1024];
                        int bytesRead;

                        do
                        {
                            bytesRead = remoteStream.Read(buffer, 0, buffer.Length);

                            localStream.Write(buffer, 0, bytesRead);

                            bytesProcessed += bytesRead;
                        }

                        while (bytesRead > 0);  
                    }
                }                
            }
            catch(Exception e)
            {
                string excepetion;
                //return excepetion = e.Message;
            }
            finally
            {
                if (response     != null) response.Close();
                if (remoteStream != null) remoteStream.Close();
                if (localStream  != null) localStream.Close();
            }

            return bytesProcessed.ToString();
        }
    }
}
