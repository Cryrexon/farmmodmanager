﻿using Ionic.Zip;
using System;
using System.IO;
using System.Windows;
using System.Windows.Forms;
using System.Linq;
using System.Xml.Linq;
using System.Collections.Generic;

namespace FarmModManagerLauncher.core
{
    class FileSystem
    {

        public string[] openDirectory(string DirectoryPath)
        {
            //string[] files = Directory.GetFiles(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\My Games\\FarmingSimulator2015\\mods", "*.zip");
            string[] files = Directory.GetFiles(DirectoryPath, "*.zip");

            return files;
        }

        public List<string[]> getModDesc(string DirectoryPath)
        {
            // Create a list to store your arrays
            List<string[]> fileInformation = new List<string[]>();

            string[] files = Directory.GetFiles(DirectoryPath, "*.zip");

            foreach (var file in files)
            {
                using (ZipFile zip = ZipFile.Read(file))
                {
                    ZipEntry modDescHandler = zip["modDesc.xml"];

                    if (modDescHandler != null)
                    {
                        if (File.Exists("tmp\\modDesc.xml"))
                        {
                            File.Delete("tmp\\modDesc.xml");
                        }

                        modDescHandler.Extract("tmp");

                        XDocument modDesc = XDocument.Load("tmp\\modDesc.xml");
                        string modTitle = null;
                        string modAuthor = null;
                        string modVersion = null;
                        string modFileName = null;

                        try
                        {
                            modTitle = modDesc.Element("modDesc").Element("title").Element("de").Value;
                            modAuthor = modDesc.Element("modDesc").Element("author").Value;
                            modVersion = modDesc.Element("modDesc").Element("version").Value;
                        }

                        catch
                        {

                        }

                        modFileName = Path.GetFileName(file);

                        // Create your array for this file
                        string[] modInformation = { modTitle, modAuthor, modVersion, modFileName };

                        // Add this to your list
                        fileInformation.Add(modInformation);

                        File.Delete("tmp\\modDesc.xml");
                    }
                }
            }
            return fileInformation;
        }
    }
}
