﻿using Ionic.Zip;
using System;
using System.IO;
using System.Windows;
using System.Windows.Forms;
using System.Linq;
using System.Xml.Linq;
using System.Collections.ObjectModel;
using System.Xml;
using System.Collections.Generic;

namespace FarmModManagerLauncher
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        int i = 0;

        public class Mod
        {
            public bool isModEnabled { set; get; }
            public string modTitle { set; get; }
            public string modFileName { set; get; }
            public string modAuthor { set; get; }
            public string modVersion { set; get; }
        }

        public MainWindow()
        {
            InitializeComponent();
            this.MaxWidth = SystemParameters.MaximizedPrimaryScreenWidth;
            this.MaxHeight = SystemParameters.MaximizedPrimaryScreenHeight;

            this.Loaded += new RoutedEventHandler(MainWindow_Loaded);
        }

        void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            core.FileSystem fs = new core.FileSystem();
            List<string[]> fileInformaton = fs.getModDesc(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) +  "\\My Games\\FarmingSimulator2015\\mods");

            foreach(var file in fileInformaton)
            {
                var list = new Mod { isModEnabled = true, modTitle = file[0], modAuthor = file[1], modVersion = file[2], modFileName = file[3]};

                dataGrid.Items.Add(list);
            }
        }


        private void MinimizeClick(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        private void MaximizeClick(object sender, RoutedEventArgs e)
        {
            double screenHeight = SystemParameters.PrimaryScreenHeight;
            string workArea = SystemParameters.WorkArea.Height.ToString();


            if(this.WindowState == WindowState.Maximized)
            {
                this.WindowState = WindowState.Normal;
            }
            else
            {
                this.WindowState = WindowState.Maximized;
                this.Left = Top = 0;
            }
        }

        private void CloseClick(object sender, RoutedEventArgs e)
        {
            Close();

        }

        private void MainWindow_OnMouseButtonLeftClick(object sender, RoutedEventArgs e)
        {
            this.DragMove();
        }
    }
}
